package com.avenuecode.javatest;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.javatest.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JavatestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JavatestApplicationTests {
	
	
	@LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

	@Test
	public void getAllProductsExcludingRelationships() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getAllProductsIncludingOnlyChildProductRelationship() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all?child=true", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getAllProductsIncludingOnlyImagesRelationship() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all?images=true", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getAllProductsIncludingBothChildAndImagesRelationship() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all?child=true&images=true", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getAllProductsExcludingRelationshipsWithProductIdentity() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all?id=1", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getAllProductsIncludingBothChildAndImagesRelationshipWithProductIdentity() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/all?id=1&child=true&images=true", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getChildProductsFromSpecificProduct() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/product/child/1", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void getImagesFromSpecificProduct() {
		ResponseEntity<List> entity = restTemplate.getForEntity("http://localhost:" + this.port + "/image/product/1", List.class);
		
		Assert.assertEquals(HttpStatus.OK, entity.getStatusCode());
	}
	
	@Test
	public void createProduct() {
		Product product = new Product();
		product.setId(6L);
		product.setName("Test Product 6");
		product.setDescription("test description POST 6");
		
		Product productParent = new Product();
		productParent.setId(1L);
		product.setParentProduct(productParent);
		
		ResponseEntity<Product> entity = restTemplate.postForEntity("http://localhost:" + this.port + "/product", product, Product.class);
		
		Assert.assertEquals(HttpStatus.CREATED, entity.getStatusCode());
	}

}
