package com.avenuecode.javatest.repository;

import org.springframework.data.repository.CrudRepository;

import com.avenuecode.javatest.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
