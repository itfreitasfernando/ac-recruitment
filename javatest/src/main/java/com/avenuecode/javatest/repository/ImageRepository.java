package com.avenuecode.javatest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.avenuecode.javatest.dto.ImageDTO;
import com.avenuecode.javatest.model.Image;

public interface ImageRepository extends CrudRepository<Image, Long> {

	@Query("SELECT new com.avenuecode.javatest.dto.ImageDTO(i.id, i.type) FROM Image i WHERE i.product.id = ?1")
	List<ImageDTO> getImagesFromProduct(Long productId);
}
