package com.avenuecode.javatest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.javatest.dto.ImageDTO;
import com.avenuecode.javatest.model.Image;
import com.avenuecode.javatest.model.Product;
import com.avenuecode.javatest.repository.ImageRepository;

@Service
public class ImageService {

	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	private ProductService productService;
	
	
	public List<ImageDTO> getImagesFromProduct(Long productId) {
		return imageRepository.getImagesFromProduct(productId);
	}
	
	/**
	 * 
	 * @param image
	 * @return null if the product doesn't exist
	 */
	public Image save(Image image) {
		
		if (image.getProduct() != null && image.getProduct().getId() != null) {
			Product product = productService.findById(image.getProduct().getId());
			if (product != null) {
				return imageRepository.save(image);
			}
		}
		
		return null;
	}
	
	public Image findById(Long id) {
		return imageRepository.findOne(id);
	}
	
	public void delete(Long id) {
		imageRepository.delete(id);
	}
	
	
	
}
