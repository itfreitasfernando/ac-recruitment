package com.avenuecode.javatest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.javatest.dto.ProductDTO;
import com.avenuecode.javatest.model.Product;
import com.avenuecode.javatest.repository.ProductRepository;
import com.avenuecode.javatest.util.JavaTestUtil;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	
	public Iterable<Product> findAll() {
		return productRepository.findAll();
	}
	
	public Product save(Product product) {
		if (product.getParentProduct() != null && product.getParentProduct().getId() != null) {
			Product productParent = findById(product.getParentProduct().getId());
			if (productParent != null) {
				return productRepository.save(product);
			} else {
				return null;
			}
		}
		return productRepository.save(product);
	}
	
	public Product findById(Long id) {
		return productRepository.findOne(id);
	}
	
	public void delete(Long id) {
		productRepository.delete(id);
	}
	
	
	public List<ProductDTO> getChildProducts(Long productParentId) {
		List<Product> listAllProducts = (List<Product>) productRepository.findAll();
		
		Map<Product, List<Product>> mapParentChildren = getParentChildren(listAllProducts);
		
		List<Product> productChild = mapParentChildren.get(findById(productParentId));
		
		return JavaTestUtil.convertListProductToListProductDTO(productChild);
	}
	
	public List<ProductDTO> getAllProductsWithParams(Long productId, Boolean getChildProducts, Boolean getImages) {
		
		List<Product> listAllProducts = (List<Product>) productRepository.findAll();
		
		Map<Product, List<Product>> mapParentChildren = getParentChildren(listAllProducts);
		
		if (productId != null && productId.longValue() != 0) {
			Product product = listAllProducts.stream()
						.filter(p -> p.getId().longValue() == productId.longValue())
						.findFirst()
						.orElse(null);
			listAllProducts.clear();
			listAllProducts.add(product);
		}
		
		List<ProductDTO> listProduct = new ArrayList<ProductDTO>();
		
		listAllProducts.forEach(p -> {
			if (mapParentChildren.containsKey(p)) {
				ProductDTO productDTO = new ProductDTO();
				productDTO.setId(p.getId());
				productDTO.setDescription(p.getDescription());
				productDTO.setName(p.getName());
				if (getImages) {
					productDTO.setImages(JavaTestUtil.convertListImagetoListImageDTO(p.getImages()) );
				}
				if (getChildProducts) {
					productDTO.setChild(JavaTestUtil.convertListProductToListProductDTO(mapParentChildren.get(p)) );
				}
				
				listProduct.add(productDTO);
			}
		});
		
		return listProduct;
	}

	private Map<Product, List<Product>> getParentChildren(List<Product> listAllProducts) {
		Map<Product, List<Product>> mapParentChildren = new HashMap<Product, List<Product>>();
		listAllProducts.forEach(p -> {
			if (p.getParentProduct() == null) {
				if (!mapParentChildren.containsKey(p)) {
					mapParentChildren.put(p, new ArrayList<>());
				}
			} else {
				if (mapParentChildren.containsKey(p.getParentProduct())) {
					List<Product> children = mapParentChildren.get(p.getParentProduct());
					if (!children.contains(p)) {
						children.add(p);
						mapParentChildren.put(p.getParentProduct(), children);
					}
				} else {
					List<Product> children = new ArrayList<Product>();
					children.add(p);
					mapParentChildren.put(p.getParentProduct(), children);
				}
			}
		});
		
		return mapParentChildren;
	}
	
	
	
}
