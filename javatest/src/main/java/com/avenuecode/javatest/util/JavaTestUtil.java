package com.avenuecode.javatest.util;

import java.util.ArrayList;
import java.util.List;

import com.avenuecode.javatest.dto.ImageDTO;
import com.avenuecode.javatest.dto.ProductDTO;
import com.avenuecode.javatest.model.Image;
import com.avenuecode.javatest.model.Product;

public class JavaTestUtil {

	public static List<ProductDTO> convertListProductToListProductDTO(List<Product> listProduct) {
		
		List<ProductDTO> listProductDTO = new ArrayList<ProductDTO>();
		
		listProduct.forEach(p -> {
			ProductDTO productDTO = new ProductDTO(p.getId(), p.getName(), p.getDescription(), convertListImagetoListImageDTO(p.getImages()) );
			listProductDTO.add(productDTO);
		});
		
		return listProductDTO;
	}
	
	public static List<ImageDTO> convertListImagetoListImageDTO(List<Image> listImage) {

		List<ImageDTO> listImageDTO = new ArrayList<ImageDTO>();
		
		listImage.forEach(i -> {
			ImageDTO ImageDTO = new ImageDTO(i.getId(), i.getType());
			listImageDTO.add(ImageDTO);
		});
		
		return listImageDTO;
	}
}
