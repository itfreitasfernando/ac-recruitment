package com.avenuecode.javatest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.avenuecode.javatest.dto.ProductDTO;
import com.avenuecode.javatest.model.Product;
import com.avenuecode.javatest.service.ProductService;

@RestController
@RequestMapping("product")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping("/child/{id}")
	public List<ProductDTO> getChildProducts(@PathVariable("id") Long productParentId) {
		return productService.getChildProducts(productParentId);
	}
	
	@GetMapping("/all")
	public List<ProductDTO> getAllProductsWithParams(
				@RequestParam(value = "id", defaultValue = "0") Long productId,
				@RequestParam(value = "child", defaultValue = "false") Boolean getChildProducts,
				@RequestParam(value = "images", defaultValue = "false") Boolean getImages) {
		return productService.getAllProductsWithParams(productId, getChildProducts, getImages);
	}
	
	
	@GetMapping("/{id}")
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Product not found")
	public Product findById(@PathVariable Long id) {
		return productService.findById(id);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		productService.delete(id);
		Product product = productService.findById(id);
		if (product == null) {
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody Product product) {

		Product result = productService.save(product);
		
		if (result == null) {
			return ResponseEntity.noContent().build();
		}
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@PostMapping
	public ResponseEntity<?> addProduct(@RequestBody Product product) {
		
		Product result = productService.save(product);
		
		if (result == null) {
			return ResponseEntity.noContent().build();
		}
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
}
