package com.avenuecode.javatest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.avenuecode.javatest.dto.ImageDTO;
import com.avenuecode.javatest.model.Image;
import com.avenuecode.javatest.service.ImageService;

@RestController
@RequestMapping("image")
public class ImageController {

	@Autowired
	private ImageService imageService;
	
	@GetMapping("/product/{id}")
	public List<ImageDTO> getImagesFromProduct(@PathVariable("id") Long productId) {
		return imageService.getImagesFromProduct(productId);
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		imageService.delete(id);
		Image image = imageService.findById(id);
		if (image == null) {
			return ResponseEntity.ok().build();
		}
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody Image image) {

		Image result = imageService.save(image);
		
		if (result == null) {
			return ResponseEntity.noContent().build();
		}
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@PostMapping
	public ResponseEntity<?> addImage(@RequestBody Image image) {
		
		Image result = imageService.save(image);
		
		if (result == null) {
			return ResponseEntity.noContent().build();
		}
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
}
