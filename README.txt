----------------------
Architecture and technologies used
----------------------

The architecture and technologies used were intended to be as simple and light as possible. The framework used in this application were:
-JavaEE 7
-JDK 1.8
-Spring Boot
-H2 as an in memory databases. (Spring Boot has very good integration for H2)

----------------------
Build, tests and run
----------------------

The easiest way to build, test and run the application is, with Maven installed and cofigured, go to --javatest-- directory using command prompt and type 
the following command:

 -mvn spring-boot:run

After this go to URL Address or you can use any API Development Environment such as Postman. All calls are described as follow:


**Get all products excluding relationships (child products, images)**
URL:  /product/all
Method: GET
Sample Call: http://localhost:8080/product/all


**Get all products including specified relationships (child product and/or images) **
URL:  /product/all?{child}&{images}
Method: GET
URL Params: 
	Optional: child=[boolean], images=[boolean]
Sample Call: http://localhost:8080/product/all?child=true&images=true
	     http://localhost:8080/product/all?images=true
             http://localhost:8080/product/all?child=true


** Get all products excluding relationships (child products, images) using specific product identity **
URL:  /product/all?{id}
Method: GET
URL Params: 
	Optional: id[Long]
Sample Call: http://localhost:8080/product/all?id=1


** Get all products including specified relationships (child product and/or images)  using specific product identity **
URL:  /product/all?{id}&{child}&{images}
Method: GET
URL Params: 
	Optional: id[Long], child=[boolean], images=[boolean]
Sample Call: http://localhost:8080/product/all?id=1&child=true&images=true
             http://localhost:8080/product/all?id=1&child=true
	     

** Get set of child products for specific product **
URL:  /product/child/:id
Method: GET
URL Params: 
	Required: id[Long]
Sample Call: http://localhost:8080/product/child/1


** Get set of images for specific product **
URL:  /image/product/:id
Method: GET
URL Params: 
	Required: id[Long]
Sample Call: http://localhost:8080/image/product/1


** Create product **
URL:  /product
Method: POST
Sample Call: http://localhost:8080/product
	Body: "id": 6,
	      "name": "test description POST 6",
          "description": "test POST 6",
          "listImages": [],
          "parentProduct": {
                 "id": 1	        
           }

** Update product **
URL:  /product
Method: PUT
Sample Call: http://localhost:8080/product
	Body: "id": 6,
	      "name": "update teste description POST 6",
          "description": "update teste POST 6",
          "listImages": [],
          "parentProduct": 
                 "id": 1	        
               }

** Delete product **
URL:  /product/:id
Method: DELETE
URL Params: 
	Required: id[Long]
Sample Call: http://localhost:8080/product/1

** Create image **
URL:  /image
Method: POST
Sample Call: http://localhost:8080/image
	Body: "id": 1,
        
	      "type": "type image",
                
              "product": {
	        
                 "id": 1	        
	    
               }

** Update image **
URL:  /image
Method: PUT
Sample Call: http://localhost:8080/image
	Body: "id": 1,
        
	      "type": "UPDATE type image",
                
              "product": {
	        
                 "id": 1	        
	    
               }

** Delete image **
URL:  /image/:id
Method: DELETE
URL Params: 
	Required: id[Long]
Sample Call: http://localhost:8080/image/1

----------------------
tests
----------------------

To run the entire unit test, issue this command :

 -mvn test
